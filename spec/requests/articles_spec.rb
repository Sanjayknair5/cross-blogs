require 'rails_helper'

RSpec.describe "Articles", type: :request do

  before :example , :create_aticles => true do
     30.times { create(:article)}
  end

  describe 'POST #create' do
    context 'with invalid params' do
      it 'returns an unprocessable entity response' do
        post articles_path, params: { article: { title: '', content: '', email: '' }, format: :json }
        expect(response).to have_http_status(400)
      end
    end

    context 'with valid params' do
      it 'returns an reponse with created status' do
        post articles_path, params: { article: { title: 'test title', content: 'test content', email: 'test@crossover.com' }, format: :json }
        expect(response).to have_http_status(201)
      end
    end
  end

  describe 'GET #search' do
    context 'with valid params' do
      it 'returns an unprocessable entity response', create_aticles: true do
        get search_articles_path, params: { keyword: "test"}
        expect(response).to have_http_status(200)
        articles = JSON.parse(response.body)["articles"]
        expect(articles.count).to eq(20)
      end
    end
  end  

  describe 'PATCH #update' do
    context 'with invalid params' do
      it 'returns an unprocessable entity response' do
        article = create(:article)
        patch article_path(article), params: { article: { title: '', content: 'test content', email: 'test@crossover.com' }, format: :json }
        expect(response).to have_http_status(400)
      end
    end

    context 'with valid params' do
      it 'returns OK status' do
        article = create(:article)
        patch article_path(article), params: { article: { title: 'test comment', content: 'test content', email: 'test@crossover.com' }, format: :json }
        expect(response).to have_http_status(200)
      end
    end
  end  


  describe 'DELETE #destroy' do
    context 'with valid params' do
      it 'returns an unprocessable entity response' do
        article = create(:article)
        delete article_path(article)
        expect(response).to have_http_status(200)
      end
    end
  end


end

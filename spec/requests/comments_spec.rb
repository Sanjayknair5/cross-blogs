require 'rails_helper'

RSpec.describe "Comments", type: :request do

  before :example , :create_article => true do
     @article = create(:article)
  end

  before :example , :create_comments => true do
     30.times { create(:comment, {article: @article})}
  end

  describe 'POST #create' do
    context 'with invalid params' do
      it 'returns an unprocessable entity response', create_article: true do
        post article_comments_path(@article), params: { comment: { article_id: @article.id, title: '', content: '', email: '' }, format: :json }
        expect(response).to have_http_status(400)
      end
    end

    context 'with valid params' do
      it 'returns an reponse with created status', create_article: true do
        post article_comments_path(@article), params: { comment: { article_id: @article.id, title: 'test title', content: 'test comment', email: 'test@crossover.com' }, format: :json }
        expect(response).to have_http_status(201)
      end
    end
  end

  describe 'GET #index' do
    context 'with valid params' do
      it 'returns an unprocessable entity response', create_article: true, create_comments: true do

        get article_comments_path(@article.id)
        expect(response).to have_http_status(200)
        comments = JSON.parse(response.body)["comments"]
        expect(comments.count).to eq(30)
      end
    end
  end  

end
